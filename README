ALPHA
---------
This core's status is alpha. It is not fit for everyday use or packaging.

Geolith
-------
Geolith is an emulator for the Neo Geo AES and MVS.

This emulator supports TerraOnion's .NEO file format only.

BIOS files from a recent MAME set are required:
 - aes.zip for Neo Geo AES (Home Console)
 - neogeo.zip for Neo Geo MVS (Arcade) and Unibios

This repository lives at https://gitlab.com/jgemu/geolith

Compiling
---------
Make sure you have The Jolly Good API's header files installed. If you did
not install them, you will be required to include their path in CFLAGS.

Options:
  USE_EXTERNAL_MINIZ -  Set non-zero to use an external miniz
  USE_VENDORED_SPEEXDSP - Set to a non-zero value to use vendored SpeexDSP

Linux:
  make

macOS:
  make

BSD:
  gmake

Windows (MSYS2):
  make

The build will be output to "geolith/". This directory may be used as is
locally by copying it to your local "cores" directory, or may be installed
system-wide using the "install" target specified in the Makefile.

Notes
-----
This project began life as a fork of GnGeo 0.8.1. All original components
were removed or replaced as the project progressed, with the resulting codebase
being entirely new. All non-commercial and GPL code has been removed, and all
new code has been written under the BSD 3-Clause license. This process has
similarities to the creation of the BSD operating system itself, which was
built over time by replacing all components of AT&T UNIX with new, permissively
licensed ones. With this being stated, a special thanks goes out to Mathieu
Peponas who both created the original GnGeo emulator, and also approved the
distribution of this new work as a whole with new licensing. This resolves the
"Grandfather's Axe" or "Ship of Theseus" question.

Copyright
---------
Geolith (BSD-3-Clause)
  Copyright (c) 2022-2023 Rupert Carmichael
  See LICENSE

Jolly Good Z80 (MIT)
  Copyright (c) 2019 Nicolas Allemand
  Copyright (c) 2020-2022 Rupert Carmichael
  Copyright (c) 2022 rofl0r
  See src/z80/LICENSE

miniz (MIT)
  Copyright 2013-2014 RAD Game Tools and Valve Software
  Copyright 2010-2014 Rich Geldreich and Tenacious Software LLC
  See deps/miniz/miniz.c (https://github.com/richgel999/miniz)

Musashi (MIT)
  Copyright (c) 1998-2019 Karl Stenerud
  See src/m68k/readme.txt (https://github.com/kstenerud/Musashi)

Speex Resampler (BSD-3-Clause)
  Copyright (c) 2003, 2007, 2008 Xiph.org Foundation
  See source files in deps/speex/ (https://github.com/xiph/speexdsp)

YMFM-C (BSD-3-Clause)
  Copyright (c) 2021 Aaron Giles
  Copyright (c) 2022 Rupert Carmichael
  See source files in src/ymfm
